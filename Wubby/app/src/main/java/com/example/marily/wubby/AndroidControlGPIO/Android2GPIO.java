package com.example.marily.wubby.AndroidControlGPIO;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.marily.wubby.R;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UTFDataFormatException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Android2GPIO extends AppCompatActivity {


    boolean powerflag,flag =false,connect = true;
    /*connect flag changes depending on the state of the connection.
    We will use it to prevent a user from tapping the on/off switch while its disconnected
    */
    EditText ip1,ip2,ip3,ip4;
    Button button, handler,connx,disconnx;
    TextView tv,contv;
    String ip = "";
    Dialog dialog;
    ImageView btn;
    Spinner spinner;
    int dstPort = 8013;

    LinearLayout lin;


    public ArrayAdapter<String> adapter;
    ArrayList<String> list;
    public MySQLiteHelper db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.android2gpio);



        ip1 = (EditText)findViewById(R.id.ip1);
        ip2 = (EditText)findViewById(R.id.ip2);
        ip3 = (EditText)findViewById(R.id.ip3);
        ip4 = (EditText)findViewById(R.id.ip4);
        button = (Button) findViewById(R.id.button);
        btn = (ImageView)findViewById(R.id.btn);


        tv = (TextView)findViewById(R.id.tv);
        contv = (TextView)findViewById(R.id.contv);
        spinner = (Spinner)findViewById(R.id.spinner);
        lin = (LinearLayout)findViewById(R.id.linLayout);
        handler = (Button) findViewById(R.id.ipHandler);
        connx = (Button)findViewById(R.id.Connx);
        disconnx = (Button)findViewById(R.id.disconnx);



        //============== type a new ip ==================
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateDialog();
            }
        });

        db = new MySQLiteHelper(this);
        list = new ArrayList<String>();
        if (db!=null){
            db.getAll(list);//get all previous ip's from database


        }


        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,list);

        spinner.setAdapter(adapter);




        //============= delete Ip ===================
        handler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //delete all contents from database and adapter
                db.deleteAll(list);
                adapter.clear();
                adapter.notifyDataSetChanged();


            }
        });

        //===== connect with the selected ip from spinner=====
        connx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isConnected Conn = new isConnected();
                Conn.execute(new String[]{ip});
            }
        });


     //==================== ON/OFF ================================

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if ((!ip.contains("[0-9].[0-9].[0-9].[0-9]") && flag == false)||(!connect)){

                    Toast.makeText(getApplicationContext(),"No Ip provided", Toast.LENGTH_SHORT).show();


                }else{

                    if (powerflag == true) {//If powerflag = true, then led is off and we turn it on
                        SocketClass my_socket = new SocketClass();
                        my_socket.execute("1");


                    }else{//else it is on and we turn it off
                        SocketClass my_socket = new SocketClass();
                        my_socket.execute("0");

                    }
                }



            }

        });

        //================ Disconnect ========================

        disconnx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SocketClass disconnx = new SocketClass();
                disconnx.execute("disconnect");
            }
        });

        //================ Spinner Selection ==================

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                /*Spinner item selection initiates always once at the beginning of the app
                 *so we increment count and for count >1 means that we passed the first initiation of the
                 *listener
                 */




                    System.out.println("selected item:"+spinner.getSelectedItem().toString());

                    ip = spinner.getSelectedItem().toString();


                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                Toast.makeText(getApplicationContext(),"No Ip stored",Toast.LENGTH_LONG).show();
            }
        });


    }




     //==============================================================================//
    //==we use isConnected class to establish that the given ip is the correct one==//
   //==============================================================================//
    private class isConnected extends AsyncTask<String, Void ,String> {

        String ipAdr, response;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog =new Dialog(Android2GPIO.this);
            dialog.setContentView(R.layout.dialog);
            dialog.show();

            TextView tvdial = (TextView)dialog.findViewById(R.id.tvdial);
            tvdial.setText("Connecting...");
            tvdial.setTextSize(30);
        }

        @Override
        protected String doInBackground(String ... params) {
            ip = params[0];
            ipAdr = params[0];
            System.out.println("isConnected ip: " + ipAdr);

            try {

                Socket client = new Socket(ipAdr, dstPort);

                InputStream inputStream = client.getInputStream();  //open the input stream
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);


                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);// set the reader of the message
                StringBuilder stringBuilder = new StringBuilder();
                String bufferedString;

                while ((bufferedString = bufferedReader.readLine()) != null) { //while we havent reached the end of the message
                    // keep appending the rest of the message on the string


                    stringBuilder.append(bufferedString);
                    System.out.println("still in while");
                }

                bufferedString = stringBuilder.toString();

                System.out.println("after while");
                response = stringBuilder.toString();
                System.out.println("response:"+response);

                client.close();

            } catch (UnknownHostException e) {
                e.printStackTrace();
                System.out.println("unknown host");
            } catch (IOException e) {
                e.printStackTrace();
                //einai IO to la8os
                System.out.println("IO");
                dialog.cancel();
                return "Wrong device IP";



            }
            dialog.cancel();
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);



            System.out.println("result" + s);
            if (s.contains("0")) {

                System.out.println("Connected");


                tv.setText("OFF");
                powerflag = true;
                contv.setText("CONNECTED");
                lin.setVisibility(View.GONE);
                disconnx.setVisibility(View.VISIBLE);
                flag = true;
                connect = true;
            }else if (s.contains("1")){

                System.out.println("Connected");
                powerflag = false;
                tv.setText("ON");
                contv.setText("CONNECTED");
                lin.setVisibility(View.GONE);
                disconnx.setVisibility(View.VISIBLE);
                flag = true;
                connect = true;

            }else{

                Toast.makeText(getApplicationContext(),"Wrong device IP. \n Press Connect button to type IP again", Toast.LENGTH_LONG).show();
            }
        }
    }

      //==============================================//
     //============ CUSTOM ALERT DIALOG =============//
    //==============================================//
    public void onCreateDialog(){



        final android.support.v7.app.AlertDialog.Builder builder =  new android.support.v7.app.AlertDialog.Builder(this) ;

        //Define which layout the dialog will use
        LayoutInflater factory = LayoutInflater.from(this);
        final View ipText = factory.inflate(R.layout.ip, null);
        builder.setView(ipText);


        //set picture
        builder.setIcon(R.drawable.ic_settings_remote_black_24dp);
        //set Title
        builder.setTitle("Enter Ip");


        ip1 = (EditText)ipText.findViewById(R.id.ip1);
        ip2 = (EditText)ipText.findViewById(R.id.ip2);
        ip3 = (EditText)ipText.findViewById(R.id.ip3);
        ip4 = (EditText)ipText.findViewById(R.id.ip4);

        ip1.setInputType(InputType.TYPE_CLASS_NUMBER);
        ip2.setInputType(InputType.TYPE_CLASS_NUMBER);
        ip3.setInputType(InputType.TYPE_CLASS_NUMBER);
        ip4.setInputType(InputType.TYPE_CLASS_NUMBER);


        builder.setCancelable(false)
                .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ip = ip1.getText().toString() + "." + ip2.getText().toString() + "." + ip3.getText().toString() + "." + ip4.getText().toString();
                        System.out.println("ip:" + ip);

                        if ((ip1.getText().toString().matches("")) || (ip2.getText().toString().matches("")) || (ip3.getText().toString().matches(""))
                                || (ip4.getText().toString().matches(""))) {

                            //checking if the user left any input field void
                            Toast.makeText(getApplicationContext(), "Incorrect IP input", Toast.LENGTH_SHORT).show();

                        } else {

                            ip.replaceAll("\\s+", "");//remove the gaps that might be




                            isConnected Conn = new isConnected();
                            Conn.execute(new String[]{ip});

                            //Insert new ip into spinner and database
                            adapter.add(ip);
                            db.addIp(ip);
                        }
                    }


                });



        builder.show();

    }





      //===============================================================
     //========================= ON / OFF ============================
    //===============================================================
    public class SocketClass extends AsyncTask<String,Void ,String> {
        String ipAdr,response;
        @Override
        protected String doInBackground(String... params) {
            ipAdr =ip ;
            System.out.println("socket class:"+ipAdr);
            try
            {


                Socket client = new Socket(ipAdr, dstPort);

                //======== OUTPUT STREAM ========//
                DataOutputStream out =new DataOutputStream( client.getOutputStream()); //open the output stream

                out.writeBytes(params[0]);                                             //sending on/off command

                client.shutdownOutput(); //closing the output stream
                System.out.println("outputStream disconnected");


                //========= INPUT STREAM =========//
                InputStream inputStream = client.getInputStream();  //open the input stream
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);// set the reader of the message
                StringBuilder stringBuilder = new StringBuilder();

                String bufferedString;


                while ((bufferedString = bufferedReader.readLine()) != null) { //while we havent reached the end of the message
                // keep appending the rest of the message on the string
                    stringBuilder.append(bufferedString);
                    System.out.println("still in while");
                }
                response = stringBuilder.toString();


                System.out.println("out of while!");
                System.out.println("response:"+response); //print the incoming message

                client.close(); //close the socket
                System.out.println("socket closed ");


            } catch (EOFException e){

                e.printStackTrace();

            } catch (UTFDataFormatException e){

                e.printStackTrace();
            }catch (UnknownHostException e) {
                e.printStackTrace();
                System.out.println("unknown host");
            } catch (IOException e) {
                e.printStackTrace();
                //einai IO to la8os
                System.out.println("IO");
                return "Wrong device IP";



            }
            //System.out.println(response);
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            tv.setText(result);

            if (result.contains("ON")) {
                powerflag = false;
            } else if (result.contains("OFF")) {

                powerflag = true;


            } else {


                //dstPort = Integer.parseInt(result);

                lin.setVisibility(View.VISIBLE);
                disconnx.setVisibility(View.GONE);
                tv.setText("");
                contv.setText("DISCONNECTED");
                connect=false;

            }

        }

    }

    /*when for some reason the activity leaves the background
     *we will break the connection with the device
     */
    @Override
    protected void onStop() {
        super.onStop();

        SocketClass disconnx = new SocketClass();
        disconnx.execute("disconnect");

    }


}


