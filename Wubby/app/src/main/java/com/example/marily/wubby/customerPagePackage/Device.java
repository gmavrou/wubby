package com.example.marily.wubby.customerPagePackage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.example.marily.wubby.AndroidControlGPIO.Android2GPIO;
import com.example.marily.wubby.MainActivity;
import com.example.marily.wubby.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Device extends AppCompatActivity {


    boolean flag=true,uniFlag=false,delFlag;
    String devName, curName, defName,username,rmId;
    int curVersion , defVersion,devId,appId,appVersionId;
    TextView tvName,purchased, notPurchased;
    TableLayout table,table2,notTable2;
    ProgressBar spinner;
    Dialog dialog;
    public static final String broker = "tcp://5.189.168.89:1883";
    ImageView home;

    //Global Variables for MQTT
    //=========================
    String inTopic,outTopic,mqttuser, mqttpass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        Toolbar tb = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(tb);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        home = (ImageView)tb.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeInt = new Intent(Device.this, userPage.class);
                homeInt.putExtra("username",username);
                startActivity(homeInt);
            }
        });

        //Take the username from previous activity in case the user hits home button
        //==========================================================================
        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras ==null){
                username = null;
            }else{
                username= extras.getString("username");
            }
        }


        tvName = (TextView) findViewById(R.id.devname);
        table = (TableLayout) findViewById(R.id.tableLayout);
        purchased = (TextView)findViewById(R.id.purchased);
        table2 = (TableLayout) findViewById(R.id.table);
        notTable2 = (TableLayout) findViewById(R.id.table2);
        notPurchased = (TextView)findViewById(R.id.notPurchased);



        //Get the info of the device from Brow_Device.class
        //==================================================
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                devName = null;
                defName = null;
                curName = null;
                curVersion = Integer.parseInt(null);
                defVersion = Integer.parseInt(null);
                devId = Integer.parseInt(null);
            } else {
                devName = extras.getString("deviceName");
                defName = extras.getString("default AppName");
                curName = extras.getString("current AppName");
                curVersion = extras.getInt("current AppVersion");
                defVersion = extras.getInt("default AppVersion");
                devId = extras.getInt("device Id");
            }

        }
    //Get the info of the device from Brow_Device.class
    //==================================================

    //GetCurApp devices = new GetCurApp();
    //devices.execute();




//Set the infos of the device using created method Headers
        //========================================================
        tvName.setTextSize(25);
        tvName.setText(devName);
        Headers("Default app", "Default version");
        Headers(defName, String.valueOf(defVersion));
        Headers("Current app", "Current version");
        Headers(curName, String.valueOf(curVersion));
        purchased.setText("Purchased Applications");
        notPurchased.setText("Not Purchased Applications");

        MyHttpPost myApps = new MyHttpPost();
        myApps.execute();
    }



        //======================================================================
        //==================|| COMPATIBLE APPS Request ||=======================
        //======================================================================
        class MyHttpPost extends AsyncTask<String, Void, String> {

            protected void onPreExecute(){
                spinner =(ProgressBar)findViewById(R.id.progressBar1);
                spinner.setVisibility(View.VISIBLE);
            }
            @Override
            protected String doInBackground(String... params) {


                //send data

                HttpGet httpget = new HttpGet("http://"+MainActivity.url+"/wubby/customer/browseDeviceApps?deviceId="+String.valueOf(devId));
                httpget.setHeader("Accept", "application/json");
                String res = null;


                // Execute HTTP Get Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httpget);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                    //tv.setText(stringBuilder.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }


                return res;

            }

            @Override
            protected void onPostExecute(String result) {

                spinner.setVisibility(View.GONE);

                if(result.contains("purchasedAppVersionInfoList")){
                    try {
                        JSONObject obj = new JSONObject(result);
                        //Pairnoume tis info gia to mqtt pou 8eloume
                        inTopic =obj.getString("mqttTopic_pub");
                        outTopic = obj.getString("mqttTopic_sub");
                        mqttuser = obj.getString("mqttUsername");
                        mqttpass = obj.getString("mqttPwd");


                        JSONArray array = obj.getJSONArray("purchasedAppVersionInfoList");
                        Log.i("length purchased array",String.valueOf(array.length()));

                        for (int i =0; i<array.length();i++) {
                            JSONObject object = array.getJSONObject(i);
                            appId=object.getInt("appId");
                            appVersionId = object.getInt("appVersionId");

                            //Checking if the app is already installed (current app) else print it
                            if (object.getString("appName").equals(curName) && object.getString("appVersion").equals(String.valueOf(curVersion))) {
                                buildTable(object.getString("appName") , " v" + object.getString("appVersion"), "Installed", table2,appVersionId,true,appId);
                                System.out.println(object.getString("appName"));
                            } else {
                                //name =object.getString("appName");
                                buildTable(object.getString("appName") , " v" + object.getString("appVersion"), "Install", table2,appVersionId,true,appId);

                            }
                        }

                        array = obj.getJSONArray("notPurchasedAppVersionInfoList");
                        Log.i("length not purchased array",String.valueOf(array.length()));


                        for (int i =0; i<array.length();i++){
                            JSONObject object = array.getJSONObject(i);

                            //we need the appId to user it for the purchase
                            appId=object.getInt("appId");
                            Log.i("app id",String.valueOf(appId));

                            if (object.getString("appName").equals(curName) && object.getString("appVersion").equals(String.valueOf(curVersion))) {
                                buildTable(object.getString("appName") , " v" + object.getString("appVersion"), "Installed", notTable2,1,false,1);
                            } else {

                                buildTable(object.getString("appName") , " v" + object.getString("appVersion"), "Purchase", notTable2,appId,false,1);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(result=="{ }") {
                    Intent returnToMain = new Intent(Device.this,MainActivity.class);
                    returnToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(returnToMain);
                }
            }

        }

      //===================================================================================================
     //==================================|| BUILD TABLE Function ||========================================
    //=====================================================================================================
    private void buildTable(final String x , final String appV, final String y,TableLayout tablet, final int id,boolean flag,final int appId1){
        TableRow row = new TableRow(this);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        //Tsekarw na dw an h egkatesthmenh efarmogh einai mia apo tis 2 dikes mou epomenws na mporei o
        //xristis na mpei sto interface ths
        if (/*(flag == true)&&*/ (x.contains("AndroidTest")||x.contains("client"))&&(y =="Installed")){



            System.out.println("inside button tv");
            Button tv = new Button(this);
            tv.setLayoutParams(new TableRow.LayoutParams(15,
                    TableRow.LayoutParams.WRAP_CONTENT));

            tv.setPadding(0, 0, 0, 5);
            tv.setText(x + appV);
            tv.setTextSize(17);

            row.addView(tv);
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (x.contains("AndroidTest") ){
                        Intent appInt = new Intent(Device.this, Android2GPIO.class);
                        startActivity(appInt);
                        //open AndroidTest activity
                    }
                    if(x.contains("client")){
                        //open gpio2cloud app
                        PackageManager pm = getPackageManager();
                        Intent intent = pm.getLaunchIntentForPackage("com.example.giorgos.gpio2cloud");
                        startActivity(intent);
                    }
                }
            });

        }else{
            TextView tv = new TextView(this);
            tv.setLayoutParams(new TableRow.LayoutParams(40,
                    TableRow.LayoutParams.WRAP_CONTENT));

            tv.setPadding(0, 5, 0, 5);
            tv.setText(x + appV);
            tv.setTextSize(18);

            row.addView(tv);


        }



        if ( y =="Installed"){ // if the app is installed then erase the button install
           TextView btn = new TextView(this);
            btn.setLayoutParams(new TableRow.LayoutParams(15,
                    TableRow.LayoutParams.WRAP_CONTENT));

            btn.setGravity(Gravity.CENTER);
            //tv.setPadding(0, 5, 0, 5);
            btn.setTextSize(18);
            btn.setText(y);
            delFlag = true;
            row.addView(btn);

        }else {
            final Button btn = new Button(this);
            btn.setLayoutParams(new TableRow.LayoutParams(15,
                    TableRow.LayoutParams.WRAP_CONTENT));


            btn.setText(y);
            //tv.setPadding(0, 5, 0, 5);

            row.addView(btn);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (y.equals("Purchase")) {

                        Log.i("tag","Entered on click listener");
                        Log.i("app name:",x+ appV);
                        Log.i("1st app id:",String.valueOf(id));
                        myHttpPurch purchase = new myHttpPurch();
                        purchase.execute(new String(String.valueOf(id)));
                    }

                    if(y.equals("Install")){

                        System.out.println(x+ appV);
                        curName= x;
                        curVersion = Integer.parseInt(appV.replaceAll("[^0-9]", ""));
                        System.out.println("curVersion"+curVersion);
                        myMqtt myInstall = new myMqtt();
                        myInstall.execute(new String[]{String.valueOf(id), String.valueOf(devId),});

                    }
                }
            });
        }
        if (flag){
            final Button del = new Button(this);
            del.setLayoutParams(new TableRow.LayoutParams(15,
                    TableRow.LayoutParams.WRAP_CONTENT));

            del.setText("Unistall");
            //tv.setPadding(0, 5, 0, 5);

            row.addView(del);

            del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    rmId = String.valueOf(appId1);
                    delApp myDelete = new delApp();
                    myDelete.execute(new String[]{String.valueOf(id), String.valueOf(devId)});
                }
            });


        }




        tablet.addView(row);


    }
     //======================================
    //===========|| HEADERS ||===============
   //========================================
    private void Headers(String x , String y){
        TableRow row = new TableRow(this);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        TextView tv = new TextView(this);
        TextView tv2 = new TextView(this);

        tv2.setLayoutParams(new TableRow.LayoutParams(15,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv.setLayoutParams(new TableRow.LayoutParams(40,
                TableRow.LayoutParams.WRAP_CONTENT));

        tv.setTextSize(20);
        //tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setPadding(5, 5, 5, 5);


        tv.setText(x);

        tv2.setTextSize(20);
        //tv2.setTypeface(Typeface.DEFAULT_BOLD);

        tv2.setText(y);

        row.addView(tv);
        row.addView(tv2);


        table.addView(row);

    }

    //===============================================================
    //======================|| REMOVE request ||====================
    //=================================================================
    public class Remove extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            dialog = new Dialog(Device.this);
            dialog.setContentView(R.layout.dialog);
            dialog.show();

            TextView tvdial = (TextView) dialog.findViewById(R.id.tvdial);
            tvdial.setText("Removing...");
            tvdial.setTextSize(30);

        }

        @Override
        protected String doInBackground(String... params) {



            HttpPost httppost = new HttpPost("http://" + MainActivity.url + "/wubby/customer/remove");
            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {

                Log.i("2nd app id:", rmId);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("appId", rmId));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.i("exception", e.getMessage());
            }

            return res;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            dialog.cancel();
            System.out.println(s);

            if(s.contains("false")){
                Toast.makeText(getApplicationContext(),"REMOVAL FAILED\n" +
                        "The app must be unistalled from every device before it can be removed",Toast.LENGTH_LONG).show();
            }else{
                finish();
                startActivity(getIntent());
            }

        }
    }
      //===============================================================
     //======================|| PURCHASE request ||====================
    //=================================================================
    public class myHttpPurch extends AsyncTask<String, Void, String> {

          protected void onPreExecute() {
              dialog =new Dialog(Device.this);
              dialog.setContentView(R.layout.dialog);
              dialog.show();

              TextView tvdial = (TextView)dialog.findViewById(R.id.tvdial);
              tvdial.setText("Purchasing...");
              tvdial.setTextSize(30);
          }

        @Override
        protected String doInBackground(String... params) {


            HttpPost httppost = new HttpPost("http://"+MainActivity.url+"/wubby/customer/purchaseApp?deviceId="+String.valueOf(devId));
            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {

                Log.i("2nd app id:",params[0]);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("appId",params[0]));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();




                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                 Log.i("exception", e.getMessage());
            }

            return res;

        }

        @Override
        protected void onPostExecute(String result) {
            dialog.cancel();

            System.out.println(result);
            finish();
            startActivity(getIntent());
        }

    }

      //===============================================================
     //=======================|| UNISTALL request||======================
    //=================================================================
    public class delApp extends AsyncTask<String, Void, String> implements MqttCallback {

        String content = "NEW_COMMAND";
        int qos = 2;
        String client = "rcp_user";
        MemoryPersistence persistence = new MemoryPersistence();

        protected void onPreExecute() {
            dialog =new Dialog(Device.this);
            dialog.setContentView(R.layout.dialog);
            dialog.show();

            TextView tvdial = (TextView)dialog.findViewById(R.id.tvdial);
            tvdial.setText("Unistalling...");
            tvdial.setTextSize(30);
        }

        @Override
        protected String doInBackground(String... params) {



            HttpPost httppost = new HttpPost("http://" + MainActivity.url + "/wubby/customer/browseDeviceApps/sendCommand");
            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {

                Log.i("2nd app id:", params[0]);
                Log.i("deviceId:", params[1]);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("appId", params[0]));
                nameValuePairs.add(new BasicNameValuePair("deviceId", params[1]));
                nameValuePairs.add(new BasicNameValuePair("cmdId", "3"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                    MqttClient sampleClient = new MqttClient(broker, client, persistence);

                    MqttConnectOptions connOnpts = new MqttConnectOptions();


                    connOnpts.setUserName(mqttuser);
                    connOnpts.setPassword(mqttpass.toCharArray());
                    connOnpts.setCleanSession(true);

                    sampleClient.setCallback(delApp.this);

                    System.out.println("Connecting to broker: " + broker);

                    sampleClient.connect(connOnpts);
                    System.out.println("Connected!");

                    System.out.println("Subscribing to both in and out topics");
                    sampleClient.subscribe(inTopic);
                    sampleClient.subscribe(outTopic);

                    MqttMessage message = new MqttMessage();
                    message.setPayload(content.getBytes());

                    message.setQos(qos);

                    System.out.println(" inTopic: " + inTopic + " outTopic " + outTopic);
                    System.out.println("mqtt user: " + mqttuser + " mqtt pass: " + mqttpass);

                    sampleClient.publish(inTopic, message);

                    System.out.println("Publishing message: " + content);
                    int count = 0;
                    while (flag) {


                        if (count % 1000000000 == 0) {

                            System.out.println("still in while");
                        }
                        count++;
                    }//we need a loop for the entire time of the installation of the app to the device
                    //function messageArrived is executed automatically by the device so when it is installed it returns
                    //NEW_RESPONSE and with the while loop, async task wont stop until the installation is complete
                    System.out.println("Out of while!");


                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (MqttException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                 Log.i("exception", e.getMessage());
            }

            return null;

        }

        @Override
        protected void onPostExecute(String result) {



        }

        @Override
        public void connectionLost(Throwable throwable) {

        }

        @Override
        public void messageArrived(String s, MqttMessage message) throws Exception {
            System.out.println("messageArrived");
            System.out.println("message:" + message.toString());
            flag = false;
            if (message.toString().contains("NEW_RESPONSE")) {

                dialog.cancel();
                uniFlag = true;
                Response response = new Response();
                response.execute(new String[]{message.toString().replaceAll("[^0-9]", "")});


            }


        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

        }
    }

      //==============================================================================
    //=============================||INSTALL implementation ||=======================
    // ===============================================================================
    public class myMqtt extends AsyncTask<String, Void, String> implements MqttCallback {

        String content = "NEW_COMMAND";
        int qos = 2;
        String client = "rcp_user";
        MemoryPersistence persistence = new MemoryPersistence();

        protected void onPreExecute() {
            dialog =new Dialog(Device.this);
            dialog.setContentView(R.layout.dialog);
            dialog.show();

            TextView tvdial = (TextView)dialog.findViewById(R.id.tvdial);
            tvdial.setText("Installing...");
            tvdial.setTextSize(30);
        }

        @Override
        protected String doInBackground(String... params) {


            HttpPost httppost = new HttpPost("http://" + MainActivity.url + "/wubby/customer/browseDeviceApps/sendCommand");
            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {

                Log.i("Unistall app id:", params[0]);
                Log.i("deviceId:", params[1]);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("appId", params[0]));
                nameValuePairs.add(new BasicNameValuePair("deviceId", params[1]));
                nameValuePairs.add(new BasicNameValuePair("cmdId", "2"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                    MqttClient sampleClient = new MqttClient(broker, client, persistence);

                    MqttConnectOptions connOnpts = new MqttConnectOptions();


                    connOnpts.setUserName(mqttuser);
                    connOnpts.setPassword(mqttpass.toCharArray());
                    connOnpts.setCleanSession(true);

                    sampleClient.setCallback(myMqtt.this);

                    System.out.println("Connecting to broker: " + broker);

                    sampleClient.connect(connOnpts);
                    System.out.println("Connected!");

                    System.out.println("Subscribing to both in and out topics");
                    sampleClient.subscribe(inTopic);
                    sampleClient.subscribe(outTopic);

                    MqttMessage message = new MqttMessage();
                    message.setPayload(content.getBytes());

                    message.setQos(qos);

                    System.out.println(" inTopic: " + inTopic + " outTopic " + outTopic);
                    System.out.println("mqtt user: " + mqttuser + " mqtt pass: " + mqttpass);

                    sampleClient.publish(inTopic, message);

                    System.out.println("Publishing message: " + content);
                    int count = 0;
                    while (flag) {


                        if (count % 1000000000 == 0) {

                            System.out.println("still in while");
                        }
                        count++;
                    }//we need a loop for the entire time of the installation of the app to the device
                    //function messageArrived is executed automatically by the device so when it is installed it returns
                    //NEW_RESPONSE and with the while loop, async task wont stop until the installation is complete
                    System.out.println("Out of while!");


                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (MqttException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.i("exception", e.getMessage());
            }

            return null;

        }

        @Override
        protected void onPostExecute(String result) {




        }

        @Override
        public void connectionLost(Throwable throwable) {

        }

        @Override
        public void messageArrived(String inTopic, MqttMessage message) throws Exception {
            System.out.println("messageArrived");
            System.out.println("message:" + message.toString());
            flag = false;
            if (message.toString().contains("NEW_RESPONSE")) {

                dialog.cancel();

                Response response = new Response();
                response.execute(new String[]{message.toString().replaceAll("[^0-9]", "")});



            }

        }


        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

        }

    }


    class Response extends AsyncTask <String, Void, String>{



        @Override
        protected String doInBackground(String... params) {

            System.out.println(params[0]);
            HttpGet httpget = new HttpGet("http://"+ MainActivity.url+"/wubby/command/"+params[0]+"/getResponse/");
            httpget.setHeader("Accept", "application/json");
            String res = null;


            // Execute HTTP Get Request
            try {

                HttpResponse response = null;

                try {
                    response = MainActivity.httpclient.execute(httpget);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                String bufferedString;


                while ((bufferedString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(bufferedString);
                }
                res = stringBuilder.toString();


                //tv.setText(stringBuilder.toString());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }


            return res;
        }

        protected void onPostExecute(String result){
            Log.i("tag","onPostExecute");
            System.out.println(result.toString());



            if(result.toString().contains("WUBBY_OK")){

                if (uniFlag ==false) {
                    //if uniFlag is false then we pushed install button
                    getIntent().putExtra("current AppName", curName);
                    getIntent().putExtra("current AppVersion", curVersion);

                    finish();
                    startActivity(getIntent());

                }else{
                    //we pushed delete button and the device will return to the default app
                    getIntent().putExtra("current AppName",defName);
                    getIntent().putExtra("current AppVersion", defVersion);

                    System.out.println("rmId"+rmId);

                    AlertDialog.Builder dialog = new AlertDialog.Builder(Device.this);
                    dialog.setMessage("Do you want to completely remove the app from the device");
                    dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Remove remove = new Remove();
                            remove.execute(String.valueOf(rmId));
                        }
                    });
                    dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            finish();
                            startActivity(getIntent());
                        }
                    });
                    dialog.show();

                }


            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {


            case R.id.refresh:
                finish();
                startActivity(getIntent());


        }


        return super.onOptionsItemSelected(item);
    }


}
