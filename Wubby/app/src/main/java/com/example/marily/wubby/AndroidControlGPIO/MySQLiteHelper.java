package com.example.marily.wubby.AndroidControlGPIO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giorgos on 24/5/2016.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "IpDB";

    //Define Table name
    private static String TABLE_IP= "IP";

    //Define Table column names
    private static String Ip = "Ips";

    private static String[] COLUMNS = {Ip};

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }




    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_IP_TABLE = "CREATE TABLE "+ TABLE_IP+"("+Ip+" TEXT)";
        db.execSQL(CREATE_IP_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_IP);

        this.onCreate(db);
    }

    public void addIp(String ip){
        System.out.println("add IP: "+ip);

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Ip, ip);

        db.insert(TABLE_IP, null, values);

        db.close();

    }

    public void  getAll(List<String> ipList){

        String query ="SELECT * FROM "+ TABLE_IP;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        String ip = null;
        if (cursor.moveToFirst()){

            do{
                ip = cursor.getString(0);

                ipList.add(ip);
            }while (cursor.moveToNext());

        }
    }

    /*public void deleteIP(List<String> ipList,String ip){

        //get reference to writeable db
        SQLiteDatabase db = this.getWritableDatabase();

        //delete
        db.delete(TABLE_IP,Ip+"=?",new String[]{ip});

        //close
        db.close();
        System.out.println("ip "+ip+" deleted");
    }*/

    public void deleteAll(ArrayList<String> ip){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM "+TABLE_IP);

        ip.clear();
        db.close();
        System.out.println("Everyting deleted from the db");
    }


}

