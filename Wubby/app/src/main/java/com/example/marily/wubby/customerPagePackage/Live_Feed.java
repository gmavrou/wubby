package com.example.marily.wubby.customerPagePackage;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


import com.example.marily.wubby.MainActivity;
import com.example.marily.wubby.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultXAxisAxisValueFormatter;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class Live_Feed extends AppCompatActivity{

    String uuid,username;

    ImageView home;
    LineChart lineChart;

    private Timer timer;
    private TimerTask timerTask;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_feed);

        Toolbar tb = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(tb);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        home = (ImageView)tb.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeInt = new Intent(Live_Feed.this, userPage.class);
                homeInt.putExtra("username",username);
                startActivity(homeInt);
            }
        });

        if (savedInstanceState==null){

            Bundle extras = getIntent().getExtras();

            if(extras == null){
                uuid = null;
                username = null;
            }
                uuid = extras.getString("Uuid");
                username = extras.getString("username");
        }

        diagram myDiagram = new diagram();
        myDiagram.execute(new String[]{uuid});



    }

    class diagram extends AsyncTask<String, Void, String>{

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... params) {

            System.out.println("deviceId:"+params[0]);

            HttpGet httpget= new HttpGet("http://"+MainActivity.url+"/wubby/customer/getLiveFeedData?device_uuid="+params[0]+"&feed_name=testFeed&datastream_name=testDatastream");
            httpget.setHeader("Accept", "application/json");
            String res= null;

            // Execute HTTP Get Request
            try {
                Log.i("tag3", "before httpGet");
                HttpResponse response = null;

                try {
                    response = MainActivity.httpclient.execute(httpget);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("tag4", "after httpGet");

                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                String bufferedString;


                while ((bufferedString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(bufferedString);
                }
                res = stringBuilder.toString();


                //tv.setText(stringBuilder.toString());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }


            return res;

        }

        protected void onPostExecute(String result){

            //See result first
            System.out.println(result.toString());

            try {
                JSONObject json = new JSONObject(result);


                JSONArray xArray = json.getJSONArray("xdata");
                JSONArray yArray = json.getJSONArray("ydata");

                System.out.println(xArray.length());
                System.out.println(yArray.toString());

                lineChart = (LineChart)findViewById(R.id.chart);

                ArrayList<Entry> entries = new ArrayList<>();
                for (int i = 0;i <yArray.length();i++){

                    entries.add(new Entry(yArray.getInt(i), i));

                }

                LineDataSet dataSet = new LineDataSet(entries, "Live Feed");
                lineChart.getXAxis().setValueFormatter(new DefaultXAxisAxisValueFormatter());


                ArrayList<String> labels = new ArrayList<String>();
                for (int i= 0 ;i <xArray.length();i++){
                    labels.add(xArray.getString(i));
                }

                LineData data = new LineData(labels,dataSet);
                lineChart.setData(data);



                lineChart.setDescription("");
                lineChart.setNoDataText("...");

                dataSet.setDrawCubic(true);//change the shape of the graph chart

                lineChart.invalidate();

            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    }

    public void onResume(){
        super.onResume();
        try{
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    diagram myDiagram = new diagram();
                    myDiagram.execute(new String[]{uuid});
                }
            };
            timer.schedule(timerTask,5000,5000);
            System.out.println("onResume");
        }catch (IllegalStateException e){
            android.util.Log.i("Damn", "resume error");
            System.out.println("onResume exception");
        }
    }

    public void onPause(){
        super.onPause();
        timer.cancel();
        System.out.println("onPause");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){

            case R.id.refresh:
                finish();
                startActivity(getIntent());
                break;
        }


        return super.onOptionsItemSelected(item);
    }


}
