Wubby
=========
This repository contains my Diploma Thesis that 
consists of Android projects Wubby. 

This app is a user profile where the user can sign in or log 
in the company's server (where I was intern) and browse the IoT 
devices he has registered or register a new one. Details are on the .doc
file attached to the repo.

For compiling and running wubby the steps are very easy:

* Android Studio must be installed on pc
* Clone the repo locally
* Open Android Studio and open the project Wubby
* If there are sdk tools missing and can't build the project
then Google has automated the process and has option for installation 
all the missing tools or updating the sdk and gradle components.
* After all of the above just click run and the screen for emulator 
will pop up which is preferable to choose your phone and not the virtual emulators 
because they are slower!