package com.example.marily.wubby.customerPagePackage;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;


import com.example.marily.wubby.MainActivity;
import com.example.marily.wubby.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Brow_Device extends AppCompatActivity {


    String name = "";
    String defaultApp = "";
    String current = "",uuid;

    TableLayout table_layout;
    String username;
    TextView device;
    ImageView home;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_devices);

        Toolbar tb = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(tb);
        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        home = (ImageView)tb.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        //Take the username from previous activity in case the user hits home button
        //==========================================================================
        device = (TextView) findViewById(R.id.tv);
        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras ==null){
                username = null;
            }else{
                username= extras.getString("username");
            }
        }


        MyHttpPost devices = new MyHttpPost();
        devices.execute();


    }





    class MyHttpPost extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {



            //send data

            HttpGet httpget = new HttpGet("http://"+MainActivity.url+"/wubby/customer/browseDevices");
            httpget.setHeader("Accept", "application/json");
            String res = null;


            // Execute HTTP Get Request
            try {
                Log.i("tag3", "before httpGet");
                HttpResponse response = null;

                try {
                    response = MainActivity.httpclient.execute(httpget);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("tag4", "after httpGet");

                InputStream inputStream = response.getEntity().getContent();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuilder stringBuilder = new StringBuilder();

                String bufferedString;


                while ((bufferedString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(bufferedString);
                }
                res = stringBuilder.toString();


                //tv.setText(stringBuilder.toString());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }


            return res;

        }

        @Override
        protected void onPostExecute(String result) {



            Log.i("result", result);

            int defVersion, curVersion,DeviceId;

            if (result.contains("deviceName")) {

                try {
                    JSONObject object = new JSONObject(result);
                    JSONArray json = object.getJSONArray("deviceInfoList");

                    table_layout = (TableLayout)findViewById(R.id.tableLayout1);
                    table_layout.removeAllViews();

                    Headers("Device name", "Current app");

                    for (int i = 0; i < json.length(); i++) {
                        //Device name
                        //=============
                        JSONObject obj = json.getJSONObject(i);
                        name = obj.getString("deviceName");
                        DeviceId = obj.getInt("deviceId");
                        uuid = obj.getString("deviceUuid");

                        //Default app
                        //============
                        JSONObject defObj = obj.getJSONObject("defaultApp");
                        defaultApp = defObj.getString("appName");
                        defVersion = defObj.getInt("appVersion");


                        //Current app
                        //============
                        JSONObject curObj = obj.getJSONObject("currentApp");
                        current = curObj.getString("appName");
                        curVersion = curObj.getInt("appVersion");



                        BuildTable(name,current+" v"+curVersion,name, defaultApp,defVersion,current,curVersion,DeviceId,uuid);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //device.setText("Bad request to the server!!!");

                }
                //device.setText(name +" "+defaultapp + " "+current);

            }else if(result=="{ }") {
                Intent returnToMain = new Intent(Brow_Device.this,MainActivity.class);
                returnToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(returnToMain);
            }else{
                device.setTextSize(18);
                device.setText("No devices registered yet!");
            }
        }

    }

    //Build the headers of the table for the devices
    private void Headers(String x , String y){
        TableRow row = new TableRow(this);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        TextView tv = new TextView(this);
        TextView tv2 = new TextView(this);


        tv2.setLayoutParams(new TableRow.LayoutParams(15,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv.setLayoutParams(new TableRow.LayoutParams(40,
                TableRow.LayoutParams.WRAP_CONTENT));

        tv.setTextSize(20);
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        tv.setPadding(5, 5, 5, 5);


        tv.setText(x);

        tv2.setTextSize(20);
        tv2.setTypeface(Typeface.DEFAULT_BOLD);


        tv2.setText(y);

        row.addView(tv);
        row.addView(tv2);


        table_layout.addView(row);

    }

    //Build the table with the intel for each device and push the infos in an intent for the next activity
    private void BuildTable(String x,String y, final String devName, final String defName, final int defVersion, final String curName, final int curVersion, final int devId, final String uuid) {


        // outer for loop
        //for (int i = 1; i <= rows; i++) {

            TableRow row = new TableRow(this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));

            Button btn = new Button(this);
            TextView tv = new TextView(this);

            tv.setLayoutParams(new TableRow.LayoutParams(40,
                    TableRow.LayoutParams.MATCH_PARENT));

            btn.setLayoutParams(new TableRow.LayoutParams(15,
                    TableRow.LayoutParams.WRAP_CONTENT));


            tv.setText(x);
            tv.setTextSize(18);

            btn.setText(y);
            //btn.setBackgroundColor(Color.YELLOW);


            row.addView(tv);
            row.addView(btn);

            row.setPadding(0, 50, 0, 50);

            btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    Intent mInt = new Intent(Brow_Device.this, Device.class);
                    mInt.putExtra("deviceName", devName);
                    mInt.putExtra("default AppName", defName);
                    mInt.putExtra("default AppVersion", defVersion);
                    mInt.putExtra("current AppName", curName);
                    mInt.putExtra("current AppVersion", curVersion);
                    mInt.putExtra("device Id", devId);
                    mInt.putExtra("username", username);
                    startActivity(mInt);


                }
            });

            TableRow row2 = new TableRow(this);

            row2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));

            Button live = new Button(this);
            live.setLayoutParams(new TableRow.LayoutParams(10,
                    TableRow.LayoutParams.WRAP_CONTENT));

            live.setText("Live Feed");
            row.addView(live);
            live.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent liveInt = new Intent(Brow_Device.this, Live_Feed.class);
                    liveInt.putExtra("Uuid", uuid);
                    liveInt.putExtra("username",username);
                    startActivity(liveInt);
                }
            });

        table_layout.addView(row);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //we will handle refresh here
        switch (item.getItemId()) {

            case R.id.refresh:
                finish();
                startActivity(getIntent());


        }


        return super.onOptionsItemSelected(item);
    }



}
