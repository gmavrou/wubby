package com.example.marily.wubby.customerPagePackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.marily.wubby.MainActivity;
import com.example.marily.wubby.R;

/**
 * Created by marily on 13/12/15.
 */
public class userPage extends AppCompatActivity {

    String username;
    TextView user;
    ProgressBar progressBar;
    Button devButton, regButton, logButton;
    ImageView home;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_page);

        Toolbar tb = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(tb);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        home = (ImageView)tb.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });


        //Get the username
        //=================
        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras ==null){
                username = null;
            }else{
                username= extras.getString("username");
            }
        }

        if (username == "{ }"){

            Intent returnToMain = new Intent(userPage.this,MainActivity.class);
            returnToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(returnToMain);

        }

        devButton =(Button)findViewById(R.id.devButton);
        regButton =(Button)findViewById(R.id.regButton);
        logButton = (Button)findViewById(R.id.logout);

        progressBar =(ProgressBar) findViewById(R.id.progressBar1);
        user = (TextView)findViewById(R.id.xristis);
        user.setText(username);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Intent regInt = new Intent(userPage.this,Reg_Device.class);
                regInt.putExtra("username",username);
                startActivity(regInt);
                progressBar.setVisibility(View.GONE);

            }
        });

        devButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Intent browInt = new Intent(userPage.this, Brow_Device.class);
                browInt.putExtra("username",username);
                startActivity(browInt);
                progressBar.setVisibility(View.GONE);

            }
        });

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logoutInt = new Intent(userPage.this, MainActivity.class);
                logoutInt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(logoutInt);
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){


            case R.id.refresh:
                finish();
                startActivity(getIntent());

        }


        return super.onOptionsItemSelected(item);
    }


}
