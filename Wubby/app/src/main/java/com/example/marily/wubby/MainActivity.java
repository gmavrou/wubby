package com.example.marily.wubby;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.marily.wubby.customerPagePackage.userPage;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText user,pass;
    Button logButton,regButton;
    TextView message;
    public static DefaultHttpClient httpclient;
    private ProgressBar spinner;
        public static final String url="5.189.168.89:8080";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        //getSupportActionBar().setDisplayShowHomeEnabled(true); prepei na dw ti kanoun aftes oi 2 entoles
        //getSupportActionBar().setIcon(R.drawable.wubby);



        user=(EditText) findViewById(R.id.user);
        pass=(EditText)findViewById(R.id.password);
        logButton = (Button)findViewById(R.id.button);
        message=(TextView) findViewById(R.id.message);
        regButton = (Button) findViewById(R.id.button2);


        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyHttpPost login = new MyHttpPost();
                login.execute();
            }
        });

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent regInt = new Intent(MainActivity.this, Reg_Customer.class);
                startActivity(regInt);
            }
        });

    }





    class MyHttpPost extends AsyncTask<String, Void, String> {

        String username;

        @Override
        protected void onPreExecute() {
            spinner =(ProgressBar)findViewById(R.id.progressBar1);
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            username = user.getText().toString();
            String password = pass.getText().toString();

            //send data
            httpclient = new DefaultHttpClient();


            HttpPost httppost = new HttpPost("http://"+url+"/wubby/login");



            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {

                Log.i("tag2", "Entered value pairs");
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("username",username));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {
                    Log.i("tag3", "before httpPost");
                    HttpResponse response = null;

                    try {
                        response = httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();

                        return "No internet connection!";
                    }

                    Log.i("tag4", "after httpPost");

                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                    //tv.setText(stringBuilder.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.i("exception", e.getMessage());
            }

            return res;

        }

        @Override
        protected void onPostExecute(String result) {


            spinner.setVisibility(View.GONE);

            Log.i("result", result);

            if (result.contains("role")) {

                try {
                    JSONObject obj = new JSONObject(result);


                    Intent userInt = new Intent(MainActivity.this, userPage.class);
                    userInt.putExtra("username", username);
                    startActivity(userInt);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                message.setText("");

            }else if(result.contains("No internet connection!")){

                message.setText("No internet connection!");
            }else {
                message.setText("Invalid username and password!");


            }
        }

    }





}
