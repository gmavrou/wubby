package com.example.marily.wubby;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Reg_Customer extends AppCompatActivity {

    EditText user,pass,conPass, email;
    Button back,regButton;
    TextView tv;
    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_customer);



        user=(EditText) findViewById(R.id.user);
        pass=(EditText)findViewById(R.id.pass);
        conPass = (EditText)findViewById(R.id.conPass);
        email = (EditText)findViewById(R.id.email);
        tv=(TextView) findViewById(R.id.tv);
        regButton = (Button) findViewById(R.id.button);
        back = (Button) findViewById(R.id.backButton);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RegPost register = new RegPost();
                register.execute();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




    }
    //idia akrivws logikh me to login
    public class RegPost extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            spinner =(ProgressBar)findViewById(R.id.progressBar1);
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {

            String username = user.getText().toString();
            String password = pass.getText().toString();
            String conPassword = conPass.getText().toString();
            String mail =  email.getText().toString();

            //send data
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://"+MainActivity.url+"/wubby/registerCustomer");
            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {


                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("password1",conPassword));
                nameValuePairs.add(new BasicNameValuePair("email",mail));
                nameValuePairs.add(new BasicNameValuePair("username",username));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return "No internet connection";
                    }



                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();


                    //tv.setText(stringBuilder.toString());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                Log.i("exception", e.getMessage());
            }

            return res;

        }

        @Override
        protected void onPostExecute(String result) {

            spinner.setVisibility(View.GONE);
            Log.i("result", result);

            if (result.contains("Username is already in use!")) {

                tv.setText("Username is already in use!");


            }else if (result.contains("> 5 characters!")){
                tv.setText("Username and password should contain more than 5 characters!!!");

            }else if(result.contains("Please specify a valid password"))
            {
                tv.setText("Please specify a valid password");
            }

            else if(result.contains("Please confirm your password"))
            {
                tv.setText("Please confirm your password");
            }
            else if(result.contains("Please specify an email address!"))
            {
                tv.setText("Please specify an email address");
            }

            else if(result.contains("The passwords do not match"))
            {
                tv.setText("The passwords do not match!");
            }
            else if(result.contains("Invalid e-mail"))
            {
                tv.setText("Invalid e-mail!");
            }else if(result.contains("No internet connection")){

                tv.setText("No internet connection!");
            }
            else  {

                tv.setText("You have been registered succesfully, you can now login!");


            }
        }

    }
}

