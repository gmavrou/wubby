package com.example.marily.wubby.customerPagePackage;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.marily.wubby.MainActivity;
import com.example.marily.wubby.R;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marily on 14/12/15.
 */
public class Reg_Device extends AppCompatActivity {

    TextView msg;
    EditText uuid;
    Button regButton;
    String username;
    Toolbar actionbarToolbar ;
    ImageView home;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_device);

        Toolbar tb = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(tb);

        final android.support.v7.app.ActionBar ab = getSupportActionBar();

        // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        home = (ImageView)tb.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //Take the username from previous activity in case the user hits home button
        //==========================================================================
        if (savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras ==null){
                username = null;
            }else{
                username= extras.getString("username");
            }
        }

        msg = (TextView) findViewById(R.id.register);
        msg.setMovementMethod(new ScrollingMovementMethod());

        regButton = (Button) findViewById(R.id.button);
        uuid  = (EditText)findViewById(R.id.serial);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegPost register = new RegPost();
                register.execute();
            }
        });

    }
    public class RegPost extends AsyncTask<String, Void, String> {

        String serial;
        @Override
        protected String doInBackground(String... params) {

            serial = uuid.getEditableText().toString();


            //send data
            Log.i("uuid", serial);

            HttpPost httppost = new HttpPost("http://"+MainActivity.url+"/wubby/customer/registerDevice");

            httppost.setHeader("Accept", "application/json");
            String res = null;
            try {


                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("deviceUuid",serial));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                try {

                    HttpResponse response = null;

                    try {
                        response = MainActivity.httpclient.execute(httppost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                    InputStream inputStream = response.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedString;


                    while ((bufferedString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedString);
                    }
                    res = stringBuilder.toString();




                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                 Log.i("exception", e.getMessage());
            }

            return res;

        }

        @Override
        protected void onPostExecute(String result) {

            Log.i("result", result);

            msg.setText(result.toString());
            if (result.contains("succesfully")) {

                msg.setText("The device with UUID " + serial + " registered succesfully");

            }else if (result.contains("already registered")) {

                msg.setText("The device with UUID " + serial + " is already registered!");

            }else if (result.contains("Please specify a valid UUID")){

                msg.setText("Please specify a valid UUID!");

            }else {
                msg.setText("Failed to register device with UUID "+serial+"!");

            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){

            case R.id.refresh:
                finish();
                startActivity(getIntent());
                break;
        }


        return super.onOptionsItemSelected(item);
    }

}
